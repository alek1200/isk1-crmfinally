﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Users
{
    public abstract class AbstractUserVM
    {
        [Key]
        [Required]
        [DisplayName("ID użytkownika")]
        public int UserId { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        [DisplayName("Login")]
        public string Login { get; set; }
    }
}