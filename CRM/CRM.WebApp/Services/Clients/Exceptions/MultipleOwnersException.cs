﻿using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Services.Clients.Exceptions
{
    public class MultipleOwnersException : Exception
    {
        public ServiceRequest ServiceRequest { get; private set; }
        public IEnumerable<IClient> Owners { get; private set; }

        public MultipleOwnersException(ServiceRequest request, IEnumerable<IClient> owners)
            : base()
        {
            ServiceRequest = request;
            Owners = owners;
        }
    }
}