﻿using CRM.WebApp.Models;
using CRM.WebApp.Models.Applications;
using CRM.WebApp.Services.Functions.Contracts;
using CRM.WebApp.Services.ServiceRequests.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CRM.WebApp.Services.Functions
{
    public class FileHandlerSrv : IFileHandlerSrv
    {
        ICrmDbContext _dbContext;

        public FileHandlerSrv(ICrmDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool AddDocument(HttpPostedFileBase document)
        {
            try
            {
                _dbContext.Documents.Add(ConvertToDocument(document));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddDocument(Document document)
        {
            try
            {
                _dbContext.Documents.Add(document);

                _dbContext.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Document ConvertToDocument(HttpPostedFileBase file)
        {
            try
            {
                Document doc = null;
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var contentType = file.ContentType;
                    var contentLength = file.ContentLength;

                    byte[] data = new byte[] { };
                    using (var binaryReader = new BinaryReader(file.InputStream))
                    {
                        data = binaryReader.ReadBytes(file.ContentLength);
                    }

                    doc = new Document()
                    {
                        FileName = fileName,
                        ContentType = contentType,
                        ContentLength = contentLength,
                        Data = data
                    };
                }
                return doc;
            }
            catch (Exception)
            {
                throw new Exception("Error in converting a document");
            }
        }

        public Document[] Documents
        {
            get { return _dbContext.Documents.ToArray(); }
        }

        public Document FindDocument(int id)
        {
            return _dbContext.Documents.Find(id);
        }

        public IQueryable<Document> GetDocument(Expression<Func<Document, bool>> expression)
        {
            return _dbContext.Documents.Where(expression);
        }

        public bool RemoveDocumet(int id)
        {
            try
            {
                var query = from d in _dbContext.Documents where d.DocumentId == id select d;
                Document doc = query.Single();
                _dbContext.Documents.Remove(doc);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }






    }
}