﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.WebApp.Models.Applications;

namespace CRM.WebApp.Models.Clients
{
    public class Company : IClient
    {
        public Company()
        {
            ServiceRequests = new List<ServiceRequest>();
            Complaints = new List<Complaint>();
        }

        public int CompanyId { get; set; }

        [StringLength(40, MinimumLength = 3), Required]
        [DisplayName("Nazwa firmy")]
        public string CompanyName { get; set; }

        [StringLength(10), Required]
        [DisplayName("NIP")]
        public string NIP { get; set; }

        public virtual ContactDetails ContactDetails { get; set; }

        public virtual Address ClientAddress { get; set; }

        public virtual ICollection<ServiceRequest> ServiceRequests { get; set; }

        public virtual ICollection<Complaint> Complaints { get; set; }

        #region IClient members

        string prefix = "c";

        string IClient.ClientId
        {
            get
            {
                return prefix + CompanyId;
            }
            set
            {
                CompanyId = int.Parse(value.Substring(prefix.Length));
            }
        }

        string IClient.Name
        {
            get
            {
                return CompanyName;
            }
        }

        ICollection<ServiceRequest> IClient.ServiceRequests
        {
            get
            {
                return ServiceRequests;
            }
            set
            {
                ServiceRequests = value;
            }
        }

        ICollection<Complaint> IClient.Complaints
        {
            get
            {
                return Complaints;
            }
            set
            {
                Complaints = value;
            }
        }

        #endregion
    }
}