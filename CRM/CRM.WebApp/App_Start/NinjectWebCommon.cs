[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CRM.WebApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CRM.WebApp.App_Start.NinjectWebCommon), "Stop")]

namespace CRM.WebApp.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using CRM.WebApp.Services.Clients;
    using CRM.WebApp.Models;
    using CRM.WebApp.Services.Users;
    using CRM.WebApp.Services.ServiceRequests.Contracts;
    using CRM.WebApp.Services.ServiceRequests;
    using CRM.WebApp.Services.Functions.Contracts;
    using CRM.WebApp.Services.Functions;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICrmDbContext>().To<CrmDbContext>().InRequestScope();
            //    kernel.Bind<IClientDbContext, IAddressDbContext, IContactDetailsDbContext, IUsersDbContext>().To<CrmDbContext>().InSingletonScope();
            //    kernel.Bind<IServiceRequestDbContext, IDocumentDbContext, IMailDbContext>().To<CrmDbContext>().InSingletonScope();
            kernel.Bind<IImapClientSrv>().To<ImapClientSrv>();
            kernel.Bind<ISmtpClientSrv>().To<SmtpClientSrv>();
            kernel.Bind<IPeopleSrv, ICompaniesSrv, IClientSrv>().To<ClientSrv>();
            kernel.Bind<IServiceRequestSrv>().To<ServiceRequestSrv>();
            kernel.Bind<IUsersSrv>().To<UserSrv>();
            kernel.Bind<IFileHandlerSrv>().To<FileHandlerSrv>();
        }
    }
}
