﻿using CRM.WebApp.Models.Clients;

namespace CRM.WebApp.Services.Clients
{
    public interface IClientSrv
    {
        IClient AddClient(IClient client);
        IClient FindClient(string id);
        bool EditClient(IClient modifiedClient);
        bool RemoveClient(IClient clientToRemove);
    }

    public interface IClientSrv<T> where T : IClient
    {
        T AddClient(T client);
        T FindClient(int id);
        bool EditClient(IClient modifiedClient);
        bool RemoveClient(int id);
    }
}
