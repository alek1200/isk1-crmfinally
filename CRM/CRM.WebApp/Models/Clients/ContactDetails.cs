﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRM.WebApp.Models.Clients
{
    public class ContactDetails
    {
        public int ContactDetailsId { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [StringLength(20,MinimumLength=7)]
        [DisplayName("Numer telefonu")]
        public string PhoneNr { get; set; }

    }
}