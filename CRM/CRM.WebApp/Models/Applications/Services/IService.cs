﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Models.Applications.Services
{
    public interface IService : IDescribable
    {
        int ServiceId { get; set; }
    }
}
