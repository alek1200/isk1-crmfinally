﻿using System.Collections.Generic;
using System.Web;

using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;

namespace CRM.WebApp.ViewModel
{
    public class CreateServiceRequestVM
    {
        public ServiceRequest ServiceRequest { get; set; }
        public IEnumerable<HttpPostedFileBase> Files { get; set; } 
        public IClient Client { get; set; }
    }
}