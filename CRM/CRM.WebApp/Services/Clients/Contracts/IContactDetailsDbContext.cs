﻿using CRM.WebApp.Models.Clients;
using System.Data.Entity;

namespace CRM.WebApp.Services.Clients
{
    public interface IContactDetailsDbContext : IDbContext
    {
        IDbSet<ContactDetails> ContactsDetails { get; set; }
    }
}