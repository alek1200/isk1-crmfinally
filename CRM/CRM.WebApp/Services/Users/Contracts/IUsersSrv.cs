﻿using CRM.WebApp.Models;
using CRM.WebApp.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Services.Users
{
    public interface IUsersSrv
    {
        User Add(User userToAdd);
        bool Edit(User modifiedUser);
        User Find(int id);
        bool Remove(int id);
        IQueryable<User> GetUsers(Expression<Func<User, bool>> expression);
        User GetUser(Expression<Func<User, bool>> predicate);
        User[] Users { get; }
        bool ChangePassword(int userId, string oldPassword, string newPassword);
        void UpdateUserViewHistory<T, Y>(string userName, Y viewHistoryItem);
    }
}
