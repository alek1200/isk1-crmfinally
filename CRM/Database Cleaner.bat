SET CurrentPath=%CD%
SET ConfigFile=%CurrentPath%\CRM.WebApp\Web.config
SET MigrateExe=.\packages\EntityFramework.6.0.2\tools\migrate.exe

%MigrateExe% CRM.WebApp.dll /StartUpDirectory:%CurrentPath%\CRM.WebApp\bin\ /startUpConfigurationFile:"%ConfigFile%" /TargetMigration:0 /Force

%MigrateExe% CRM.WebApp.dll /StartUpDirectory:%CurrentPath%\CRM.WebApp\bin\ /startUpConfigurationFile:"%ConfigFile%" /Force