﻿using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Clients.Companies
{
    public class ListCompanyVM : CompanyVM      
    {
        public int ServiceRequestNumber { get; set; }
        public bool Status { get; set; }
    }
}