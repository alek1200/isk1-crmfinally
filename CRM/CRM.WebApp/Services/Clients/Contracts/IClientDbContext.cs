﻿using CRM.WebApp.Models.Clients;
using System.Data.Entity;

namespace CRM.WebApp.Services.Clients
{
    public interface IClientDbContext : IDbContext
    {
        IDbSet<Person> People { get; set; }
        IDbSet<Company> Companies { get; set; }
    }
}
