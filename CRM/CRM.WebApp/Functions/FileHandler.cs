﻿using CRM.WebApp.Models;
using CRM.WebApp.Models.Applications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CRM.WebApp.Functions
{
    public class FileHandler
    {
        public CrmDbContext _db;

        public FileHandler()
        {
            if (_db == null)
                _db = new CrmDbContext();
        }

        public Document ConvertToDocument(HttpPostedFileBase file)
        {
            try
            {
                Document doc = null;
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var contentType = file.ContentType;
                    var contentLength = file.ContentLength;

                    byte[] data = new byte[] { };
                    using (var binaryReader = new BinaryReader(file.InputStream))
                    {
                        data = binaryReader.ReadBytes(file.ContentLength);
                    }

                    doc = new Document()
                    {
                        FileName = fileName,
                        ContentType = contentType,
                        ContentLength = contentLength,
                        Data = data
                    };
                }
                return doc;
            }
            catch (Exception)
            {
                throw new Exception("Blad");
            }
        }
    }
}