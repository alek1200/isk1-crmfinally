﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CRM.WebApp.Models.Applications;

namespace CRM.WebApp.Models.Clients
{
    public interface IClient
    {
        string ClientId { get; set; }
        string Name { get; }
        ICollection<ServiceRequest> ServiceRequests { get; set; }
        ICollection<Complaint> Complaints { get; set; }
    }
}
