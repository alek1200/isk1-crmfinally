﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using CRM.WebApp.Models.Clients;
using CRM.WebApp.Functions;
using CRM.WebApp.Services;
using CRM.WebApp.Models.Applications.History;

namespace CRM.WebApp.Binders
{
    public class ClientBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var ctx = bindingContext;

            if (ctx.ModelType.IsInterface)
            {
                if (ctx.ModelType == typeof(IClient))
                {
                    var rawClientId = ctx.ValueProvider.GetValue("Id").AttemptedValue;

                    Type clientType;
                    int cId;
                    var parsingResult = ClientParsers.ParseClientIdAndType(rawClientId, out cId, out clientType);
                    if (!parsingResult)
                    {
                        throw new Exception("Parsing Client Id failed.");
                    }

                    IClient instance;
                    if (clientType == typeof(Person))
                    {
                        instance = new Person();
                    }
                    else if (clientType == typeof(Company))
                    {
                        instance = new Company();
                    }
                    else
                    {
                        throw new Exception("Unknown client type.");
                    }

                    instance.ClientId = rawClientId;
                    return instance;
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}