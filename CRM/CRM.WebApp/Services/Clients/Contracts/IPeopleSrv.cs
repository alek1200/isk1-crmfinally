﻿using CRM.WebApp.Models.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CRM.WebApp.Services.Clients
{
    public interface IPeopleSrv : IClientSrv
    {
        Person FindPerson(int id);
        IQueryable<Person> GetPeople(Expression<Func<Person, bool>> expression);
        Person[] People { get; }
        bool RemovePerson(int id);
    }
}