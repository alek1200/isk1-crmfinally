﻿using CRM.WebApp.Models.Users;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Users
{
    public class DashboardVM
    {
        public User User { get; set; }
    }
}