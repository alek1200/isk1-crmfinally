﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Users
{
    public class EditUserVM : AbstractUserVM
    {
        [StringLength(25, MinimumLength = 5)]
        [DisplayName("Stare hasło")]
        public string OldPassword { get; set; }
        [StringLength(25, MinimumLength = 5)]
        [DisplayName("Nowe hasło")]
        public string NewPassword { get; set; }
        [DisplayName("Powtórz hasło")]
        public string NewPasswordVerification { get; set; }
        [DisplayName("Administrator")]
        public bool IsAdmin { get; set; }
    }
}