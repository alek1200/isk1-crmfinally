﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CRM.WebApp.Mixins
{
    public static class ObjectExt
    {
        /// <summary>
        /// Checks objects existance.
        /// </summary>
        /// <param name="target">Compared object.</param>
        /// <returns>Returns true if reference to an object is set to null.</returns>
        public static bool IsNull(this object target)
        {
            return target == null;
        }

        /// <summary>
        /// Retrives property value from the object.
        /// </summary>
        /// <param name="obj">Object containing property value.</param>
        /// <param name="name">Property name.</param>
        /// <returns>Property value.</returns>
        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }


        /// <summary>
        /// Retrives property value from the object.
        /// </summary>
        /// <typeparam name="T">Cast object type.</typeparam>
        /// <param name="obj">Object containing property value.</param>
        /// <param name="name">Property name.</param>
        /// <returns>Property value.</returns>
        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }


        /// <summary>
        /// Compares given object model with object kept in database and get all properties within where are differencies.
        /// </summary>
        /// <param name="originalObj">Original object.</param>
        /// <param name="modifiedObj">Modified object.</param>
        /// <param name="filter">Attributes used as a filters.</param>
        /// <returns>Set of properties.</returns>
        public static ISet<string> GetChangedFields(this object originalObj, object modifiedObj, params Type[] filters)
        {
            if (originalObj.IsNull() && modifiedObj.IsNull())
                throw new ArgumentNullException("Both requests must be not null objects.");

            // Compare requests
            Type objType = originalObj.GetType();
            IEnumerable<PropertyInfo> properties = objType.GetProperties();

            // If filter attribute has been given, filter properties.
            if (!filters.IsNull())
            {
                List<PropertyInfo> filteredProperties = new List<PropertyInfo>(properties);
                foreach (Type filter in filters)
                {
                    if (filter.IsSubclassOf(typeof(Attribute)))
                        filteredProperties = filteredProperties.Where(x => !Attribute.IsDefined(x, filter)).ToList();
                    else if (filter.IsInterface)
                        filteredProperties = filteredProperties.Where(x => !x.PropertyType.GetInterfaces().Contains(filter)).ToList();
                }
                properties = filteredProperties;
            }

            // Create set of property names
            ISet<string> propertyNames = new HashSet<string>();
            foreach (var prop in properties)
                propertyNames.Add(prop.Name);

            // Compare both objects and create set of unequal fields names.
            ISet<string> diffrentProperties = new HashSet<string>();
            foreach (var name in propertyNames)
            {
                var dbReqPropertyVal = modifiedObj.GetPropValue(name);
                var modelReqPropertyVal = originalObj.GetPropValue(name);

                if (!dbReqPropertyVal.IsNull() && !dbReqPropertyVal.Equals(modelReqPropertyVal))
                    diffrentProperties.Add(name);
            }

            return diffrentProperties;
        }
    }
}