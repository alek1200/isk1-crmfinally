﻿using System.Web.Mvc;
using System.Web.Security;
using System;

using CRM.WebApp.ViewModel;
using CRM.WebApp.Models;
using CRM.WebApp.Functions;
using CRM.WebApp.Infrastructure;
using CRM.WebApp.Services.Users;
using CRM.WebApp.ViewModel.Users;
using System.Collections.Generic;
using CRM.WebApp.Models.Users;

namespace CRM.WebApp.Controllers
{
    public class UserController : Controller
    {
        /// <summary>
        /// Users service.
        /// </summary>
        IUsersSrv _userSrv;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="userSrv"></param>
        public UserController(IUsersSrv userSrv)
        {
            _userSrv = userSrv;
        }

        // GET: /User/
        public ActionResult Index()
        {
            return RedirectToAction(Constants.DefaultRedirectAction, Constants.ControllerName);
        }

        // GET: /User/
        public ActionResult Dashboard()
        {
            var loggedUser = _userSrv.GetUser(x => x.Login == User.Identity.Name);

            if (loggedUser==null)
            {
                this.ShowNotification(NotificationType.Danger, "Zalogowany użytkownik nie znaduje się w bazie danych!", true);
                return RedirectToAction("Logout");
            }

            var viewModel = new DashboardVM
            {
                User = loggedUser
            };

            return View(viewModel);
        }

        // GET: /User/Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        // POST: /User/Login
        [HttpPost]
        public ActionResult Login(User userModel)
        {
            if (ModelState.IsValid)
            {
                User user = _userSrv.GetUser(u => u.Login == userModel.Login);
                if (user == null)
                {
                    this.ShowNotification(NotificationType.Warning, "Użytkownik nie został odnaleziony w bazie danych (lub inny błąd). Spróbuj ponownie.");
                    return View(userModel);
                }

                bool correct_password = MakeHash.ReturnHashPass(userModel.Password) == user.Password;
                if (correct_password)
                {
                    // Save logged user in session.
                    Session["LoggedUser"] = user;
                    FormsAuthentication.SetAuthCookie(userModel.Login, false);
                    this.ShowNotification(NotificationType.Info, string.Format("Zalogowano jako: {0}", userModel.Login), true);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Nieprawidłowe hasło. Spróbuj ponownie.");
                }
            }
            return View();
        }

        // GET: /User/Logout
        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear(); // TODO test it!!!
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        // GET: /User/Create
        [HttpGet]
        public ActionResult Create()
        {
            CreateUserVM vm = new CreateUserVM();
            return View(vm);
        }

        // POST: /User/Create
        [HttpPost]
        public ActionResult Create(CreateUserVM vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool is_valid = Validate(vm);
                    if (is_valid)
                    {
                        // Rewrite values
                        User user = new User();
                        user.Login = vm.Login;
                        user.Password = MakeHash.ReturnHashPass(vm.Password); // Hashing      
                        user.IsAdmin = vm.IsAdmin;

                        User addedUser = _userSrv.Add(user);
                        if (addedUser == null) throw new Exception("User was not added.");

                        this.ShowNotification(NotificationType.Success, "Pomyślnie dodano nowego użytkownika.", true);
                        return Redirect(string.Format("~/{1}/Details/{0}", addedUser.UserId, Constants.ControllerName));
                    }
                }
            }
            catch (Exception) { }

            this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak, użytkownik nie został dodany.");
            return View(vm);
        }

        // /User/List
        public ActionResult List()
        {
            IEnumerable<User> users = _userSrv.Users;
            return View(users);
        }


        // GET: /User/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                throw new ArgumentNullException("ID is null.");

            User user = _userSrv.Find((int)id);
            if (user == null)
            {
                //this.ShowNotification(NotificationType.Warning, "Użytkownik o podanym ID nie został odnaleziony.");
                throw new ArgumentOutOfRangeException("User with this ID was not found.");
            }

            EditUserVM viewModel = new EditUserVM
            {
                UserId = user.UserId,
                Login = user.Login,
                IsAdmin = user.IsAdmin
            };

            return View(viewModel);
        }

        // POST: /User/Edit
        [HttpPost]
        public ActionResult Edit(EditUserVM vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Validate(vm)) // Check if passwords are correct
                    {
                        User modified_user = new User();
                        modified_user.UserId = vm.UserId;
                        modified_user.Login = vm.Login;
                        modified_user.IsAdmin = vm.IsAdmin;

                        // Check if new password was provided
                        bool new_password = !string.IsNullOrEmpty(vm.NewPassword);
                        if (new_password)
                        {
                            // Change password only
                            var pwd_change_success = _userSrv.ChangePassword(vm.UserId, vm.OldPassword, vm.NewPassword);
                            if (pwd_change_success)
                                this.ShowNotification(NotificationType.Success, "Hasło zostało zmienione", true);
                            else
                                this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak, hasło nie zostało zmienione.", true);
                        }

                        bool edit_success = _userSrv.Edit(modified_user);

                        if (edit_success)
                        {
                            this.ShowNotification(NotificationType.Success, "Użytkownik został zmieniony.", true);
                            return Redirect(string.Format("~/{1}/Details/{0}", modified_user.UserId, Constants.ControllerName));
                        }
                    }
                }
            }
            catch (Exception) { }

            this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak, użytkownik nie został zmieniony.");
            return View(vm);
        }

        /// <summary>
        /// Validates view model for create view.
        /// </summary>
        /// <param name="viewModel">View model.</param>
        /// <returns>Is Valid</returns>
        private bool Validate(CreateUserVM viewModel)
        {
            // Check passwords equality
            bool pwd_equal = viewModel.Password == viewModel.PasswordVerification;
            return pwd_equal; // && more conditions here
        }

        /// <summary>
        /// Validates view model for edit view.
        /// </summary>
        /// <param name="viewModel">View model.</param>
        /// <returns>Is Valid</returns>
        private bool Validate(EditUserVM viewModel)
        {
            // Check passwords equality (or emptiness)
            bool pwd_equal = string.IsNullOrEmpty(viewModel.NewPassword) ? true : viewModel.NewPassword == viewModel.NewPasswordVerification;
            return pwd_equal; // && more conditions here
        }

        // GET: /User/Details
        public ActionResult Details(int? id)
        {
            if (id == null) throw new ArgumentNullException("User ID is null.");

            // Retrive user from db.
            User user = _userSrv.Find((int)id);
            if (user == null) throw new NullReferenceException("User not found in database.");

            DetailsUserVM viewModel = new DetailsUserVM();
            viewModel.UserId = user.UserId;
            viewModel.Login = user.Login;
            viewModel.IsAdmin = user.IsAdmin;

            return View(viewModel);
        }
    }

    static class Constants
    {
        public const string ControllerName = "User";
        public const string DefaultRedirectAction = "List";
    }
}