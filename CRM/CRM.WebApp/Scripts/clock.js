function ShowClock() {
    var monthNames = ["Stycze�", "Luty", "Marzec", "Kwiecie�", "Maj", "Czerwiec", "Lipiec", "Sierpie�", "Wrzesie�", "Pa�dziernik", "Listopad", "Grudzie�"];
    var now = new Date;
    $('#date_bar').text("Data " + now.getFullYear() + " " + monthNames[now.getMonth()] + " " + now.getDate() + ",  " + now.getHours() + ":" + (now.getMinutes() < 10 ? "0" + now.getMinutes() : now.getMinutes()) + ":" + (now.getSeconds() < 10 ? "0" + now.getSeconds() : now.getSeconds()));
}

setInterval(function () { ShowClock() }, 1000);