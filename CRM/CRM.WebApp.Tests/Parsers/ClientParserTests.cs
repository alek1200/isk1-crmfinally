﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CRM.WebApp.Models.Clients;
using CRM.WebApp.Functions;

namespace CRM.WebApp.Tests.ServiceRequests
{
    [TestClass]
    public class ClientParserTests
    {
        [TestMethod]
        public void ParseClientIdTest()
        {
            // Arange
            string rawId = "p11203";
            int id;
            bool parsing_result;

            // Act
            parsing_result = ClientParsers.ParseClientId(rawId, out id);

            // Assert
            int expected_id = 11203;

            Assert.IsTrue(parsing_result);
            Assert.AreEqual(id, expected_id);
        }

        [TestMethod]
        public void ParseClientType()
        {
            // Arange
            string rawId = "p11203";
            Type clientType;
            bool parsing_result;

            // Act
            parsing_result = ClientParsers.ParseClientType(rawId, out clientType);

            // Assert
            Type expected_type = typeof(Person);

            Assert.IsTrue(parsing_result);
            Assert.AreEqual(clientType, expected_type);
        }


        [TestMethod]
        public void ParseCorrectPersonId()
        {
            // Arange
            string rawId = "p103";
            int id;
            Type clientType;
            bool parsing_result;

            // Act
            parsing_result = ClientParsers.ParseClientIdAndType(rawId, out id, out clientType);

            // Assert            
            int expected_id = 103;
            Type expected_type = typeof(Person);

            Assert.IsTrue(parsing_result);
            Assert.AreEqual(id, expected_id);
            Assert.AreEqual(clientType, expected_type);
        }

        [TestMethod]
        public void ParseCorrectCompanyId()
        {
            // Arange
            string rawId = "c12";
            int id;
            Type clientType;
            bool parsing_result;

            // Act
            parsing_result = ClientParsers.ParseClientIdAndType(rawId, out id, out clientType);

            // Assert            
            int expected_id = 12;
            Type expected_type = typeof(Company);

            Assert.IsTrue(parsing_result);
            Assert.AreEqual(id, expected_id);
            Assert.AreEqual(clientType, expected_type);
        }


        [TestMethod]
        public void ParseCompanyIdWithIncorrectPrefix()
        {
            // Arange
            string rawId = "cd12";
            int id;
            Type clientType;
            bool parsing_result;

            // Act
            parsing_result = ClientParsers.ParseClientIdAndType(rawId, out id, out clientType);

            // Assert            
            int expected_id = 0;
            Type expected_type = null;

            Assert.IsFalse(parsing_result);
            Assert.AreEqual(id, expected_id);
            Assert.AreEqual(clientType, expected_type);
        }

        [TestMethod]
        public void ParseCompanyIdWithNoPrefix()
        {
            // Arange
            string rawId = "12123";
            int id;
            Type clientType;
            bool parsing_result;

            // Act
            parsing_result = ClientParsers.ParseClientIdAndType(rawId, out id, out clientType);

            // Assert            
            int expected_id = 0;
            Type expected_type = null;

            Assert.IsFalse(parsing_result);
            Assert.AreEqual(id, expected_id);
            Assert.AreEqual(clientType, expected_type);
        }
    }
}
