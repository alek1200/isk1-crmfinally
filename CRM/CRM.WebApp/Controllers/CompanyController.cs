﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using PagedList;

using CRM.WebApp.Models;
using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;
using CRM.WebApp.ViewModel;
using CRM.WebApp.ViewModel.Clients.Companies;
using CRM.WebApp.Services.Clients;
using CRM.WebApp.Services.ServiceRequests.Contracts;
using CRM.WebApp.Infrastructure;
using CRM.WebApp.Services.Users;
using CRM.WebApp.Models.Users;

namespace CRM.WebApp.Controllers
{
    public class CompanyController : Controller
    {
        /// <summary>
        /// Companies service.
        /// </summary>
        private ICompaniesSrv _companiesSrv;

        /// <summary>
        /// Service requests service.
        /// </summary>
        private IServiceRequestSrv _serviceRequestSrv;



        /// <summary>
        /// Users service.
        /// </summary>
        IUsersSrv _usersSrv;

        /// <summary>
        /// Constructor.
        /// </summary>
        public CompanyController(ICompaniesSrv companiesSrv, IServiceRequestSrv serviceRequestSrv, IUsersSrv usersSrv )
        {
            _companiesSrv = companiesSrv;
            _serviceRequestSrv = serviceRequestSrv;
            _usersSrv = usersSrv;
        }

        //
        // GET: /Companies/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //
        // GET: /Companies/List
        public ActionResult List(int? page)
        {
            ICollection<ListCompanyVM> viewModel = new List<ListCompanyVM>();
            foreach (var company in _companiesSrv.Companies)
            {
                ListCompanyVM vm = new ListCompanyVM();
                vm.Company = company;
                vm.Address = company.ClientAddress;
                vm.ContactDetails = company.ContactDetails;
                vm.ServiceRequestNumber = company.ServiceRequests.Count;

                List<ServiceRequest> requests = new List<ServiceRequest>(company.ServiceRequests);
                if (requests.Exists(r => r.Status == ServiceRequestStatus.InProgress || r.Status == ServiceRequestStatus.Await))
                {
                    vm.Status = false;
                }
                else
                {
                    vm.Status = true;
                }

                viewModel.Add(vm);
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(viewModel.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Companies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = _companiesSrv.FindCompany((int)id);

            if (company == null)
            {
                return HttpNotFound();
            }

            CompanyVM vm = new CompanyVM()
            {
                Company = company,
                Address = company.ClientAddress,
                ContactDetails = company.ContactDetails
            };

            _usersSrv.UpdateUserViewHistory<CompanyViewHistoryItem, Company>(User.Identity.Name, company);

            return View(vm);
        }

        //
        // GET: /Companies/Create
        public ActionResult Create()
        {
            CompanyVM vm = new CompanyVM()
            {
                Company = new Company(),
                Address = new Address(),
                ContactDetails = new ContactDetails()
            };

            return View(vm);
        }

        //
        // POST: /Companies/Create
        [HttpPost]
        public ActionResult Create(Company company, Address address, ContactDetails contactDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    company.ClientAddress = address;
                    company.ContactDetails = contactDetails;
                    _companiesSrv.AddClient(company);

                this.ShowNotification(NotificationType.Success, "Klient został pomyślnie dodany.", true);

                    return RedirectToAction("List");
                }
                else
                {
                    throw new Exception("Model is invalid.");
                }
               
            }
            catch
            {
                this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Klient nie został dodany.");

                CompanyVM Company = new CompanyVM()
                {
                    Company = company,
                    Address = address,
                    ContactDetails = contactDetails
                };

                return View(Company);
            }
        }

        // GET: /Company/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Company company = _companiesSrv.FindCompany((int)id);

            if (company == null)
            {
                return HttpNotFound();
            }

            CompanyVM Company = new CompanyVM()
            {
                Company = company,
                Address = company.ClientAddress,
                ContactDetails = company.ContactDetails
            };

            return View(Company);
        }

        //
        // POST: /Company/Edit
        [HttpPost]
        public ActionResult Edit(Company company, Address address, ContactDetails contactDetails)
        {
            if (ModelState.IsValid)
            {
                company.ClientAddress = address;
                company.ContactDetails = contactDetails;
                bool result = _companiesSrv.EditClient(company);
                if (result) this.ShowNotification(NotificationType.Success, "Klient został zmieniony.", true);
                else this.ShowNotification(NotificationType.Warning, "Podany klient nie został zmieniony.", true);
            }
            return RedirectToAction("Details", new { id = company.CompanyId });
        }

        // GET: /Company/Delete/5
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null) throw new ArgumentNullException("Id of client is null");
            bool result = _companiesSrv.RemoveCompany((int)id);
            if (result) this.ShowNotification(NotificationType.Success, "Klient został usunięty.", true);
            else this.ShowNotification(NotificationType.Warning, "Podany klient nie został usunięty.", true);
            return RedirectToAction("List");
        }
    }
}