﻿using NLog;

namespace CRM.WebApp.Infrastructure
{
    /// <summary>
    /// Provides basic logging functionality.
    /// </summary>
    public static class Log
    {
        private class Constants
        {
            internal const string DefaultInvokerName = "CRM.WebApp";
        }

        public enum LogLevel
        {
            Debug, Error, Fatal, Info, Trace, Warn
        }

        public static void Write(string message, object invoker, LogLevel level)
        {
            string invokerName = invoker.GetType().Name;

            // Set logger instance
            Logger logger = null;
            if (string.IsNullOrWhiteSpace(invokerName))
                logger = NLog.LogManager.GetLogger(Constants.DefaultInvokerName);
            else
                logger = NLog.LogManager.GetLogger(invokerName);

            switch (level)
            {
                case LogLevel.Debug:
                    logger.Debug(message);
                    break;
                case LogLevel.Error:
                    logger.Error(message);
                    break;
                case LogLevel.Fatal:
                    logger.Fatal(message);
                    break;
                case LogLevel.Info:
                    logger.Info(message);
                    break;
                case LogLevel.Trace:
                    logger.Trace(message);
                    break;
                case LogLevel.Warn:
                    logger.Warn(message);
                    break;
                default:
                    break;
            }
        }
        public static void Debug(string message, object invoker)
        {
            Write(message, invoker, LogLevel.Debug);
        }
        public static void Error(string message, object invoker)
        {
            Write(message, invoker, LogLevel.Error);
        }
        public static void Fatal(string message, object invoker)
        {
            Write(message, invoker, LogLevel.Fatal);
        }
        public static void Trace(string message, object invoker)
        {
            Write(message, invoker, LogLevel.Trace);
        }
        public static void Warn(string message, object invoker)
        {
            Write(message, invoker, LogLevel.Warn);
        }
        public static void Info(string message, object invoker)
        {
            Write(message, invoker, LogLevel.Info);
        }
    }
}