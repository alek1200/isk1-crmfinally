﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM.WebApp.ViewModel.Users
{
    public class CreateUserVM : AbstractUserVM
    {
        [Required]
        [StringLength(25, MinimumLength = 5)]
        [DisplayName("Hasło")]
        public string Password { get; set; }
        [Required]
        [DisplayName("Powtórz hasło")]
        public string PasswordVerification { get; set; }
        [Required]
        [DisplayName("Administrator")]
        public bool IsAdmin { get; set; }
    }
}