﻿using CRM.WebApp.Models.Applications;
using System.Data.Entity;


namespace CRM.WebApp.Services.Functions.Contracts
{
    public interface IDocumentDbContext : IDbContext
    {
        IDbSet<Document> Documents { get; set; }
    }
}
