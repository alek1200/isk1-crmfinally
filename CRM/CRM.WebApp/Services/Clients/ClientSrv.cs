﻿using CRM.WebApp.Functions;
using CRM.WebApp.Models.Clients;
using CRM.WebApp.Mixins;

using System;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;
using CRM.WebApp.Models;

namespace CRM.WebApp.Services.Clients
{
    public class ClientSrv : IClientSrv, IPeopleSrv, ICompaniesSrv
    {
        /// <summary>
        /// Database context
        /// </summary>
        ICrmDbContext _dbContext;

        /// <summary>
        /// Constructor
        /// </summary>
        public ClientSrv(ICrmDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Adds new client to repository.
        /// </summary>
        /// <param name="clientToAdd"></param>
        /// <returns></returns>
        public IClient AddClient(IClient clientToAdd)
        {
            if (clientToAdd == null) throw new ArgumentNullException("Client to add is null.");

            if (clientToAdd is Person)
            {
                _dbContext.People.Add(clientToAdd as Person);
            }
            else if (clientToAdd is Company)
            {
                _dbContext.Companies.Add(clientToAdd as Company);
            }
            else throw new UnknownClientTypeException(clientToAdd);

            _dbContext.SaveChanges();

            return clientToAdd; // `Find or Add` Pattern
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="modifiedClient"></param>
        public bool EditClient(IClient modifiedClient)
        {
            if (modifiedClient == null) throw new ArgumentNullException("Argument is null.");

            bool editing_result = false;
            if (modifiedClient is Person)
            {
                editing_result = EditPerson(modifiedClient as Person);
            }
            else if (modifiedClient is Company)
            {
                editing_result = EditCompany(modifiedClient as Company);
            }
            else throw new UnknownClientTypeException(modifiedClient);

            return editing_result;
        }

        /// <summary>
        /// Edits company details. 
        /// </summary>
        /// <param name="company">Modified instance of company entry.</param>
        /// <returns>Transaction result.</returns>
        protected bool EditPerson(Person person)
        {
            if (person == null) throw new ArgumentNullException("Person is null");
            if (person.PersonId <= 0) throw new Exception("Invalid person id.");

            bool editing_result = false;

            try
            {
                // Retrive original company from context.
                var query = from p in _dbContext.People where p.PersonId == person.PersonId select p;
                query.Include(p => p.ClientAddress).Include(p => p.ContactDetails); // Load up address and contact details too.
                Person originalPerson = query.Single();
                var originalPersonEntity = _dbContext.Entry(originalPerson);
                // Apply modifications to the company.
                originalPersonEntity.CurrentValues.SetValues(person);

                editing_result = true; // Only for company!

                editing_result = EditContactDetails(person.ContactDetails, originalPerson.ContactDetails, false);
                editing_result = EditClientAddress(person.ClientAddress, originalPerson.ClientAddress, false);

                _dbContext.SaveChanges();

            }
            catch (Exception e) { editing_result = false; }

            // change editing result
            return editing_result;
        }


        /// <summary>
        /// Edits company details.
        /// </summary>
        /// <param name="company">Modified instance of company entry.</param>
        /// <returns>Transcation result.</returns>
        protected bool EditCompany(Company company)
        {
            if (company == null) throw new ArgumentNullException("Company is null.");
            if (company.CompanyId <= 0) throw new Exception("Invalid company id");

            bool editing_result = false;

            try
            {
                // Retrive original company from context.
                var query = from c in _dbContext.Companies where c.CompanyId == company.CompanyId select c;
                query.Include(c => c.ClientAddress).Include(c => c.ContactDetails); // Load up address and contact details too.
                Company originalCompany = query.Single();
                var originalCompanyEntity = _dbContext.Entry(originalCompany);
                // Apply modifications to the company.
                originalCompanyEntity.CurrentValues.SetValues(company);

                editing_result = true; // Only for company!

                editing_result = EditContactDetails(company.ContactDetails, originalCompany.ContactDetails, false);
                editing_result = EditClientAddress(company.ClientAddress, originalCompany.ClientAddress, false);

                _dbContext.SaveChanges();

            }
            catch (Exception e) { editing_result = false; }

            // change editing result
            return editing_result;

        }

        /// <summary>
        /// Edits entry containing contact details of the client.
        /// </summary>
        /// <param name="modifiedDetails">Contact details with modifications.</param>
        /// <param name="originalDetails">Original contact details (w/o modifications). When not provided, will be downloaded from repository.</param>
        /// <param name="saveOnEnd">Does the method should save changes in database on the end.</param>
        /// <returns>Returns true when edditing was successful.</returns>
        protected bool EditContactDetails(ContactDetails modifiedDetails, ContactDetails originalDetails = null, bool saveOnEnd = true)
        {
            if (modifiedDetails == null) throw new ArgumentNullException("Modified contact details are null.");

            // If no original details were provided, retrive it from repository.
            if (originalDetails == null)
            {
                var query = from d in _dbContext.ContactsDetails
                            where d.ContactDetailsId == modifiedDetails.ContactDetailsId
                            select d;
                originalDetails = query.Single();
            }
            var originalEntry = _dbContext.Entry(originalDetails);

            bool editing_result = false;
            try
            {
                // Rewrite values to the original object.
                originalEntry.CurrentValues.SetValues(modifiedDetails);

                if (saveOnEnd)
                {
                    _dbContext.SaveChanges(); // Save changes in database.
                }

                // Editing was successful, return true.
                editing_result = true;
            }
            catch (Exception) { }

            return editing_result;
        }



        /// <summary>
        ///  Edits entry containing contact details of the client.
        /// </summary>
        /// <param name="modifiedAddress">Address with modifications.</param>
        /// <param name="originalAddress">Original address. When not provided, will be downloaded from repository.</param>
        /// <param name="saveOnEnd">Does the method should save changes in database on the end.</param>
        /// <returns>Returns true when edditing was successful.</returns>
        protected bool EditClientAddress(Address modifiedAddress, Address originalAddress = null, bool saveOnEnd = true)
        {
            if (modifiedAddress == null) throw new ArgumentNullException("Modified contact details are null.");

            // If no original details were provided, retrive it from repository.
            if (modifiedAddress == null)
            {
                var query = from a in _dbContext.Addresses
                            where a.AddressId == modifiedAddress.AddressId
                            select a;
                originalAddress = query.Single();
            }
            var originalEntry = _dbContext.Entry(originalAddress);

            bool editing_result = false;
            try
            {
                // Rewrite values to the original object.
                originalEntry.CurrentValues.SetValues(modifiedAddress);

                if (saveOnEnd)
                {
                    _dbContext.SaveChanges(); // Save changes in database.
                }

                // Editing was successful, return true.
                editing_result = true;
            }
            catch (Exception) { }

            return editing_result;
        }


        /// <summary>
        /// Gets client from database. Give id should contains client type and id itself. If the client is not found in database, returns null.
        /// </summary>
        /// <param name="rawClientId">Id and its type.</param>
        /// <returns>Clients instance. If client not found, returns null.</returns>
        public IClient FindClient(string id)
        {
            try
            {
                // Retrive client id;
                int clientId;
                Type clientType;
                bool parsingResult = ClientParsers.ParseClientIdAndType(id, out clientId, out clientType);

                if (!parsingResult) throw new Exception("Parsing client id was unsuccessful!");

                // Retrive client
                IClient client;
                if (clientType == typeof(Person))
                    client = _dbContext.People.Find(clientId);
                else if (clientType == typeof(Company))
                    client = _dbContext.Companies.Find(clientId);
                else throw new UnknownClientTypeException(clientType);

                return client;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        /// <summary>
        /// Removes client from the repository.
        /// </summary>
        /// <param name="clientToRemove">Client to be removed.</param>
        /// <returns>Returns true if the client was successfuly removed.</returns>
        public bool RemoveClient(IClient clientToRemove)
        {
            if (clientToRemove == null) throw new ArgumentNullException("Client is null.");
            int id;
            ClientParsers.ParseClientId(clientToRemove.ClientId, out id);
            if (clientToRemove is Person)
            {
                return RemovePerson(id);
            }
            else if (clientToRemove is Company)
            {
                return RemoveCompany(id);
            }
            else throw new UnknownClientTypeException(clientToRemove);
        }

        public bool RemovePerson(int id)
        {
            bool removing_result = false;
            try
            {
                var query = from p in _dbContext.People where p.PersonId == id select p;
                Person person = query.Single();
                _dbContext.People.Remove(person);
                _dbContext.SaveChanges();
                removing_result = true;
            }
            catch (Exception) { }
            return removing_result;
        }

        public bool RemoveCompany(int id)
        {
            bool removing_result = false;
            try
            {
                var query = from c in _dbContext.Companies where c.CompanyId == id select c;
                Company person = query.Single();
                _dbContext.Companies.Remove(person);
                _dbContext.SaveChanges();
                removing_result = true;
            }
            catch (Exception) { }
            return removing_result;
        }


        public Person FindPerson(int id)
        {
            return _dbContext.People.Find(id);
        }

        public Company FindCompany(int id)
        {
            return _dbContext.Companies.Find(id);
        }

        public IQueryable<Person> GetPeople(Expression<Func<Person, bool>> expression)
        {
            return _dbContext.People.Where(expression);
        }

        public Person[] People
        {
            get { return _dbContext.People.ToArray(); }
        }

        public IQueryable<Company> GetCompanies(Expression<Func<Company, bool>> expression)
        {
            return _dbContext.Companies.Where(expression);
        }

        public Company[] Companies
        {
            get { return _dbContext.Companies.ToArray(); }
        }

    }
}