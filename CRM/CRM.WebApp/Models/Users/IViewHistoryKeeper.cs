﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Models.Users
{
    public interface IViewHistoryKeeper<T, Y>
    {
        void Update(Y item);
        ICollection<T> UserViewHistory { get; }
    }
}
