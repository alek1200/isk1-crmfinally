﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CRM.WebApp.Services.Users.Rights;
using CRM.WebApp.Mixins;

namespace CRM.WebApp.Services.Users.Roles
{
    public class Administrator : UserRole
    {
        UserManagmentRights _userManagmentrights;
        ServiceRequestRights _serviceRequestRights;

        public Administrator()
        {
            _userManagmentrights.Add(UserManagmentRights.CreateUser);
            _userManagmentrights.Add(UserManagmentRights.EditEveryProfile);
            _userManagmentrights.Add(UserManagmentRights.EditMyProfile);
            _userManagmentrights.Add(UserManagmentRights.RemoveUser);
            _userManagmentrights.Add(UserManagmentRights.SetUserRights);
            _userManagmentrights.Add(UserManagmentRights.ShowEveryProfile);
            _userManagmentrights.Add(UserManagmentRights.ShowMyProfile);
            
        }

        public override UserManagmentRights UserManagmentRights
        {
            get { throw new NotImplementedException(); }
        }

        public override ServiceRequestRights ServiceRequestRights
        {
            get { throw new NotImplementedException(); }
        }
    }
}