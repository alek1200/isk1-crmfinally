﻿using CRM.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Services.Functions.Contracts
{
    public interface IMailDbContext : IDbContext
    {
        IDbSet<Mail> Mails { get; set; }
    }
}
