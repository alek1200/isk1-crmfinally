﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Clients;
using CRM.WebApp.Models.Applications.Services;
using CRM.WebApp.Models.Applications.History;
using CRM.WebApp.Services.Clients;
using CRM.WebApp.Services.Users;
using CRM.WebApp.Services.ServiceRequests.Contracts;
using CRM.WebApp.Services.Functions.Contracts;
using CRM.WebApp.Models.Users;

namespace CRM.WebApp.Models
{
    public interface ICrmDbContext : IClientDbContext, IAddressDbContext, IContactDetailsDbContext, IUsersDbContext, IServiceRequestDbContext, IDocumentDbContext, IMailDbContext
    {

    }

    public class CrmDbContext : DbContext, ICrmDbContext
    {
        public CrmDbContext()
            : base("CrmDbContext")
        {

        }

        // TODO opisać zbiory

        public IDbSet<Address> Addresses { get; set; }
        public IDbSet<Company> Companies { get; set; }
        public IDbSet<Complaint> Complaints { get; set; }
        public IDbSet<Person> People { get; set; }
        public IDbSet<ServiceRequest> ServiceRequests { get; set; }
        public IDbSet<User> Users { get; set; }
        public IDbSet<Repair> Repairs { get; set; }
        public IDbSet<Analyze> Analysis { get; set; }
        public IDbSet<Maintenance> Maintanence { get; set; }
        public IDbSet<ContactDetails> ContactsDetails { get; set; }
        public IDbSet<RequestHistory> RequestHistories { get; set; }
        public IDbSet<Mail> Mails { get; set; }
        public IDbSet<Document> Documents { get; set; }
        public IDbSet<ServiceRequestViewHistoryItem> ServiceReqestViewHistoryItems { get; set; }
        public IDbSet<PersonViewHistoryItem> PersonViewHistoryItems { get; set; }
        public IDbSet<CompanyViewHistoryItem> CompanyViewHistoryItems { get; set; }

        #region RequestHistoryItems

        public IDbSet<RequestCreate> RequestCreateHistoryItems { get; set; }
        public IDbSet<RequestFieldChange> RequestFieldChangeItems { get; set; }
        public IDbSet<RequestStatusChange> RequestStatusChangeItems { get; set; }
        public IDbSet<RequestFinish> RequestFinishHistoryItems { get; set; }
        public IDbSet<RequestFieldsChange> RequestFieldsChangeItems { get; set; }

        #endregion        
    }
}