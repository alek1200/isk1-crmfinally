﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using PagedList;

using CRM.WebApp.Models.Clients;
using CRM.WebApp.Models;
using CRM.WebApp.ViewModel;
using CRM.WebApp.Services.Clients;
using CRM.WebApp.Infrastructure;
using CRM.WebApp.ViewModel.Clients.People;
using CRM.WebApp.Services.ServiceRequests.Contracts;
using CRM.WebApp.Models.Applications;
using CRM.WebApp.Models.Users;
using CRM.WebApp.Services.Users;

namespace CRM.WebApp.Controllers
{
    public class PersonController : Controller
    {
        /// <summary>
        /// People service.
        /// </summary>
        private IPeopleSrv _peopleSrv;

        /// <summary>
        /// Service requests service.
        /// </summary>
        private IServiceRequestSrv _serviceRequestSrv;

        /// <summary>
        /// Users service.
        /// </summary>
        private IUsersSrv _userSrv;


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="context"></param>
        public PersonController(IPeopleSrv peopleSrv, IServiceRequestSrv serviceRequestSrv, IUsersSrv userSrv)
        {
            _peopleSrv = peopleSrv;
            _serviceRequestSrv = serviceRequestSrv;
            _userSrv = userSrv;
        }

        // GET: /Persons/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //GET: /Persons/List
        public ActionResult List(int? page)
        {
            ICollection<ListPersonVM> viewModel = new List<ListPersonVM>();
            foreach (var person in _peopleSrv.People)
            {
                ListPersonVM vm = new ListPersonVM();
                vm.Person = person;
                vm.Address = person.ClientAddress;
                vm.ContactDetails = person.ContactDetails;
                vm.ServiceRequestNumber = person.ServiceRequests.Count;

                List<ServiceRequest> requests = new List<ServiceRequest>(person.ServiceRequests);
                if (requests.Exists(r => r.Status == ServiceRequestStatus.InProgress || r.Status == ServiceRequestStatus.Await))
                {
                    vm.Status = false;
                }
                else
                {
                    vm.Status = true;
                }

                viewModel.Add(vm);
            }



            int pageSize = 10; //On one page can be 10 elements
            int pageNumber = (page ?? 1); //Default page is 1

            return View(viewModel.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Persons/Details/5

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Person person = _peopleSrv.FindPerson((int)id);

            if (person == null)
            {
                return HttpNotFound();
            }

            PersonVM vm = new PersonVM()
            {
                Person = person,
                Address = person.ClientAddress,
                ContactDetails = person.ContactDetails
            };

            _userSrv.UpdateUserViewHistory<PersonViewHistoryItem, Person>(User.Identity.Name, person);


            return View(vm);
        }

        // GET: /Persons/Create
        public ActionResult Create()
        {
            PersonVM vm = new PersonVM()
            {
                Person = new Person(),
                Address = new Address(),
                ContactDetails = new ContactDetails()
            };

            return View(vm);
        }

        // POST: /Persons/Create
        [HttpPost]
        public ActionResult Create(Person person, Address address, ContactDetails contactDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    person.ClientAddress = address;
                    person.ContactDetails = contactDetails;

                    _peopleSrv.AddClient(person);

                    this.ShowNotification(NotificationType.Success, "Klient został pomyślnie dodany.", true);

                    return RedirectToAction("List");
                }
                else
                {
                    throw new Exception("Model is invalid.");
                }

            }
            catch
            {
                this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Klient nie został dodany.");

                PersonVM vm = new PersonVM()
                {
                    Person = person,
                    Address = address,
                    ContactDetails = contactDetails
                };

                return View(vm);
            }
        }

        // GET: /Person/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Person personEdit = _peopleSrv.FindPerson((int)id);

            if (personEdit == null)
            {
                return HttpNotFound();
            }

            PersonVM vm = new PersonVM()
            {
                Person = personEdit,
                Address = personEdit.ClientAddress,
                ContactDetails = personEdit.ContactDetails
            };

            return View(vm);
        }

        //
        // POST: /Person/Edit
        [HttpPost]
        public ActionResult Edit(Person person, Address address, ContactDetails contactDetails)
        {
            if (ModelState.IsValid)
            {
                person.ClientAddress = address;
                person.ContactDetails = contactDetails;
                bool result = _peopleSrv.EditClient(person);
                if (result)
                {
                    this.ShowNotification(NotificationType.Success, "Klient został zmieniony.", true);
                    return RedirectToAction("Details", new { id = person.PersonId });
                }

            }
            this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Klient nie został zmieniony.", true);
            return View(new PersonVM { Person = person, Address = address, ContactDetails = contactDetails });
        }

        // GET: /Person/Delete/5
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null) throw new ArgumentNullException("Id of client is null");
            bool result = _peopleSrv.RemovePerson((int)id);
            if (result) this.ShowNotification(NotificationType.Success, "Klient został usunięty.", true);
            else this.ShowNotification(NotificationType.Warning, "Podany klient nie został usunięty.", true);
            return RedirectToAction("List");
        }

    }
}