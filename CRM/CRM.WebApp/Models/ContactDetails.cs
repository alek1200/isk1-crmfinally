﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRM.WebApp.Models
{
    public class ContactDetails
    {
        public int ContactDetailsId { get; set; }

        [StringLength(100, MinimumLength = 3)]
        [Required]
        public string Email { get; set; }

        [Range(7,20)]
        public int PhoneNr { get; set; }

    }
}