﻿using CRM.WebApp.Models.Applications;
using CRM.WebApp.Mixins;
using CRM.WebApp.Infrastructure;
using CRM.WebApp.Models.Clients;
using CRM.WebApp.ViewModel;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRM.WebApp.Services.Clients;
using CRM.WebApp.Functions;
using CRM.WebApp.Services.ServiceRequests.Contracts;
using CRM.WebApp.Services.Functions.Contracts;
using System.Web;
using PagedList;
using CRM.WebApp.Services.Users;
using CRM.WebApp.Models.Users;

namespace CRM.WebApp.Controllers
{
    public class ServiceRequestController : Controller
    {
        /// <summary>
        /// Clients service
        /// </summary>
        private IClientSrv _clientSrv;

        /// <summary>
        /// Users service.
        /// </summary>
        private IUsersSrv _userSrv;

        /// <summary>
        /// FileHandler Service
        /// </summary>
        public IFileHandlerSrv _fileHandlerSrv;

        /// <summary>
        /// Service request service
        /// </summary>
        private IServiceRequestSrv _serviceRequestSrv;

        /// <summary>
        /// If you don't know where you should redirect, use this field.
        /// </summary>
        public const string DefaultRedirectAction = "List";

        /// <summary>
        /// Constructor
        /// </summary>
        public ServiceRequestController(IFileHandlerSrv fileHandlerSrv, IServiceRequestSrv serviceRequestSrv, IClientSrv clientSrv, IUsersSrv userSrv)
        {
            // Service request server
            if (serviceRequestSrv == null) throw new ArgumentNullException("serviceRequestSrv is null");
            else _clientSrv = clientSrv;

            // Client server
            if (clientSrv == null) throw new ArgumentNullException("clientSrv is null");
            else _serviceRequestSrv = serviceRequestSrv;

            if (fileHandlerSrv == null) throw new ArgumentNullException("FileHandler is null");
            else _fileHandlerSrv = fileHandlerSrv;

            //User server
            if (userSrv == null) throw new ArgumentNullException("UserSrv is null");
            else _userSrv = userSrv;
        }

        //
        // GET: /ServiceRequests/
        public ActionResult Index()
        {
            return RedirectToAction(DefaultRedirectAction);
        }

        //
        // GET: /ServiceRequests/Details/5
        public ActionResult Details(int? id)
        {
            ServiceRequest request = _serviceRequestSrv.Find((int)id);
            IClient owner = _serviceRequestSrv.FindOwner(request);
            if (request == null)
            {
                this.ShowNotification(NotificationType.Warning, "Zgłoszenie serwisowe o podanym ID nie istnieje.");
                return RedirectToAction(Constants.DefaultRedirectAction);
            }
            else
            {
                DetailsServiceRequestVM viewModel = new DetailsServiceRequestVM
                {
                    ServiceRequest = request,
                    Client = owner
                };

                _userSrv.UpdateUserViewHistory<ServiceRequestViewHistoryItem, ServiceRequest>(User.Identity.Name, request);

                return View(viewModel);
            }

        }

        // GET: /ServiceRequests/List
        public ActionResult List(int? page)
        {
            IEnumerable<ServiceRequest> requests = _serviceRequestSrv.ServiceRequests;

            if (requests.Count() == 0)
                this.ShowNotification(NotificationType.Info, "Nie dodano jeszcze żadnych zgłoszeń serwisowych.");

            var pageSize = 10;
            var pageNumber = (page ?? 1);

            return View(requests.ToPagedList(pageNumber, pageSize));
        }

        //
        // Get: /ServiceRequests/Create/c5
        [HttpGet]
        public ActionResult Create(string id = null)
        {
            IClient client = id != null ? _clientSrv.FindClient(id) : null;

            CreateServiceRequestVM vm = new CreateServiceRequestVM()
            {
                ServiceRequest = new ServiceRequest(),
                Client = client
            };

            return View(vm);
        }

        //
        // POST: /ServiceRequests/Create
        [HttpPost]
        public ActionResult Create([ModelBinder(typeof(CRM.WebApp.Binders.ClientBinder))] CreateServiceRequestVM viewModel)
        {
            // Because I'm a lazy biatch
            // I'm also lazy but I don't know why -Marcin
            var request = viewModel.ServiceRequest;
            var client = viewModel.Client;
            var documents = viewModel.Files;

            try
            {
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(client.ClientId))
                    {
                        this.ShowNotification(NotificationType.Danger, "Żaden klient nie został wybrany. Zgłoszenie nie mogło zostać stworzone.");
                        throw new Exception("Client Id is null or empty.");
                    }

                    // Add documents to request
                    if (documents.ElementAt(0) != null)
                    {
                        foreach (var document in documents)
                        {
                            if (request.Documents == null)
                            {
                                request.Documents = new List<Document>();
                            }
                            request.Documents.Add(_fileHandlerSrv.ConvertToDocument(document));
                        }
                    }

                    // Add service request
                    var addedRequest = _serviceRequestSrv.Add(request, client.ClientId);

                    this.ShowNotification(NotificationType.Success, "Zgłoszenie serwisowe zostało pozytywnie dodane.", true);

                    return RedirectToAction("List");
                }
                else
                {
                    throw new Exception("Model is invalid.");
                }
            }
            catch (Exception)
            {
                this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Zgłoszenie serwisowe nie zostało dodane.");
                return View(viewModel);
            }
        }

        //
        // GET: /ServiceRequests/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            ServiceRequest request = null;
            request = _serviceRequestSrv.Find((int)id);
            return View(request);
        }

        //
        // POST: /ServiceRequests/Edit
        [HttpPost]
        public ActionResult Edit(ServiceRequest request, IEnumerable<HttpPostedFileBase> files)
        {
            ServiceRequest orgReq = _serviceRequestSrv.Find(request.ServiceRequestId);

            if (ModelState.IsValid)
            {
                if (files != null && files.Count() > 0)
                {
                    foreach (var item in files)
                    {
                        request.Documents = new List<Document>(orgReq.Documents);
                        request.Documents.Add(_fileHandlerSrv.ConvertToDocument(item));
                    }
                }

                var editingResult = _serviceRequestSrv.Edit(request);
                if (editingResult.Successful)
                {
                    if (editingResult.ChangedPropertiesCount > 0)
                    {
                        this.ShowNotification(NotificationType.Success, "Zgłoszenie serwisowe zostało zmienione.", true);
                        return Redirect(string.Format("~/{1}/Details/{0}", request.ServiceRequestId, Constants.ControllerName));
                    }
                    else
                    {
                        this.ShowNotification(NotificationType.Info, "Nie wprowadzono żadnych zmian.", true);
                        return Redirect(string.Format("~/{1}/Details/{0}", request.ServiceRequestId, Constants.ControllerName));
                    }
                }
            }

            this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Zgłoszenie serwisowe nie zostało zmienione.");

            // If received request is not valid, show edit view again for necessery corrections.
            return View(request);
        }

        //
        // GET: /ServiceRequests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                throw new ArgumentNullException("ID is null");

            bool removing_successful = false;

            removing_successful = _serviceRequestSrv.Remove((int)id);

            if (removing_successful)
            {
                this.ShowNotification(NotificationType.Success, "Zgłoszenie zostało usunięte.", true);
            }
            else
            {
                this.ShowNotification(NotificationType.Warning, "Coś poszło nie tak. Zgłoszenie nie zostało usunięte.");
            }

            return Redirect(string.Format("~/{1}/Details/{0}", (int)id, Constants.ControllerName));
        }

        //
        // GET: /Document/5
        public ActionResult DownloadDocument(int documentId)
        {
            var doc = _serviceRequestSrv.GetDocument(documentId);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = doc.FileName,
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(doc.Data, doc.ContentType);
        }

        /// <summary>
        /// Set of constant values.
        /// </summary>
        static class Constants
        {
            public const string ControllerName = "ServiceRequest";
            public const string DefaultRedirectAction = "List";
        }
    }
}