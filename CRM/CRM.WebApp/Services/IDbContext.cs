﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.WebApp.Services
{
    public interface IDbContext : IDisposable
    {
        int SaveChanges();
        DbEntityEntry Entry(object entity);

    }
}
