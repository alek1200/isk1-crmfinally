﻿using CRM.WebApp.Models;
using CRM.WebApp.Models.Users;
using System.Data.Entity;

namespace CRM.WebApp.Services.Users
{
    public interface IUsersDbContext : IDbContext
    {
        IDbSet<User> Users { get; set; }
    }
}
