﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CRM.WebApp.Functions;

namespace CRM.WebApp.Tests.Hash
{
    [TestClass]
    public class HashTests
    {
        [TestMethod]
        public void ParseCorrectPersonId()
        {
            // Arange
            string pass = "admin";
            string hash_result;

            // Act
            hash_result = MakeHash.ReturnHashPass(pass);

            // Assert   

            string expected_hash = "c3fc69335201be2a3a0856e93c5e1a90472bd900cc574fc6422f6d3e3ef6181f";

            Assert.AreEqual(hash_result, expected_hash);
        }
    }
}
