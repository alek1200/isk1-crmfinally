﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CRM.WebApp.Models.Applications.Services;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.WebApp.Models
{
    public class Mail
    {
        //TODO: Summary!!

        [Required]
        [DisplayName("Id")]
        public int MailId { get; set; }

        [Required]
        [DisplayName("UId")]
        public int MailUintId { get; set; }

        [Required]
        [DisplayName("Nadawca")]
        public string From { get; set; }

        [Required]
        [DisplayName("Temat")]
        public string Subject { get; set; }

        [Required]
        [DisplayName("Zawartość")]
        public string Body { get; set; }
    }
}