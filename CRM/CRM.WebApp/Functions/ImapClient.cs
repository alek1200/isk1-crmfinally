﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using S22.Imap;
using System.Net.Mail;
using CRM.WebApp.Models;

namespace CRM.WebApp.Functions
{
    public class ImapClient
    {
        public const string _service = "imap.gmail.com";
        public const string _username = "crm.wzim.sggw@gmail.com";
        public const string _password = "Sggw100%";

        public S22.Imap.ImapClient client;
        public CrmDbContext _db;

        public ImapClient()
        {
            this.client = new S22.Imap.ImapClient
                (_service, 993, _username, _password, AuthMethod.Login, true);

            if (_db == null)
            {
                _db = new CrmDbContext();
            }
        }

        public IEnumerable<uint> GetAllUIds()
        {
            IEnumerable<uint> uIds;

            uIds = this.client.Search(SearchCondition.All());

            return uIds;
        }

        public IEnumerable<uint> GetMissingIds(IEnumerable<uint> uIds)
        {
            List<uint> missUIds = new List<uint>();

            foreach (var id in uIds)
            {
                if (!DatabaseContainsMail(id))
                {
                    missUIds.Add(id);
                }
            }
            return missUIds;
        }

        public IDictionary<uint, MailMessage> GetMissingMessages()
        {
            IEnumerable<uint> msgIds = GetMissingIds(GetAllUIds());

            Dictionary<uint, MailMessage> mailMessages = new Dictionary<uint, MailMessage>();

            foreach (var id in msgIds)
            {
                mailMessages.Add(id, client.GetMessage(id, FetchOptions.NoAttachments));
            }

            return mailMessages;
        }

        public bool AddMailsToDatabase(IDictionary<uint, MailMessage> messages)
        {
            return true;
        }

        public bool DatabaseContainsMail(uint msgId)
        {
            if (_db.Mails.Where(x => x.MailUintId == msgId).Count() > 0)
                return true;
            else
                return false;
        }

        public bool SetSeenFlag(uint UId)
        {
            try
            {
                client.SetMessageFlags(UId, "INBOX", MessageFlag.Seen);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteMessage(uint UId)
        {
            try
            {
                client.DeleteMessage(UId);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}