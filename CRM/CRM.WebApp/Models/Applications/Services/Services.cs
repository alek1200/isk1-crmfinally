﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

using CRM.WebApp.Mixins;
using System.ComponentModel.DataAnnotations.Schema;


namespace CRM.WebApp.Models.Applications.Services
{
    // TODO jak się rozrośnie to przenieść poszczególne klasy do oddzielnych plików

    public class Analyze : IService
    {
        /// <summary>
        /// Id
        /// </summary>
        public int AnalyzeId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [NotMapped]
        int IService.ServiceId
        {
            get { return AnalyzeId; }
            set { AnalyzeId = value; }
        }

        /// <summary>
        /// Comment left after analysing the issue.
        /// </summary>
        [DisplayName("Opis analizy")]
        public string Description { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.IsNull())
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            return this.GetChangedFields(obj).Count == 0;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Maintenance : IService
    {
        public int MaintenanceId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [NotMapped]
        int IService.ServiceId
        {
            get { return MaintenanceId; }
            set { MaintenanceId = value; }
        }

        [DisplayName("Opis konserwacji")]
        public string Description { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.IsNull())
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            return this.GetChangedFields(obj).Count == 0;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Replacement : IService
    {
        public int ReplacementId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [NotMapped]
        int IService.ServiceId
        {
            get { return ReplacementId; }
            set { ReplacementId = value; }
        }

        [DisplayName("Opis wymiany")]
        public string Description { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.IsNull())
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            return this.GetChangedFields(obj).Count == 0;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Repair : IService
    {
        /// <summary>
        /// Id
        /// </summary>
        public int RepairId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [NotMapped]
        int IService.ServiceId
        {
            get { return RepairId; }
            set { RepairId = value; }
        }

        [DisplayName("Opis naprawy")]
        public string Description { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.IsNull())
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            return this.GetChangedFields(obj).Count == 0;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class Recovery : IService
    {
        /// <summary>
        /// Id
        /// </summary>
        public int RecoveryId { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        [NotMapped]
        int IService.ServiceId
        {
            get { return RecoveryId; }
            set { RecoveryId = value; }
        }

        [DisplayName("Opis odzyskiwania")]
        public string Description { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.IsNull())
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            return this.GetChangedFields(obj).Count == 0;
        }
    }
}